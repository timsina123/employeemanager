const numberInput=document.getElementById('number').value.replace(/\D/g,'');
const button=document.getElementById('button');
const  response=document.querySelector('.response');

button.addEventListener('click',sendData,false);

const socket=io();
socket.on('smsStatus',function (data){
    response.innerHTML='<h5> Verification Code Sent To :'+data.number+'</h5>'
}
);

function sendData() {
    fetch('/application/verify',{
        method:'post',
        headers:{
            'Content-type':'application/json'
        },
        body:JSON.stringify({number:numberInput,text:'hello'})

    })
        .then(function (res) {
            console.log(res);
        })
        .catch(function (err) {
            console.log(err);
        })
}