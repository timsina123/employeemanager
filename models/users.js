const mongoose=require('mongoose');
const bcrypt=require('bcrypt');
const SALT_WORK_FACTOR=10;
const userSchema=mongoose.Schema({
   userName:{
       type:String,
       required:true
   } ,
password:{
       type:String,
    required:true,
    unique:true
},
    mobileNumber:{
       type:Number,
        required:true

    },

    created_date:{
       type:Date,
        default:Date.now()
    }
});
userSchema.pre('save', function(next) {
    var user = this;
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // hashing password
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            // over writing previous password with hash
            user.password = hash;
            next();
        });
    });

});

var Employee=module.exports=mongoose.model('employees',userSchema);

// add a new user to mongodb

module.exports.addUser=function (user,callback) {
    Employee.create(user,callback);
}

// fetching users

module.exports.getUsers=function (callback,limit) {
    Employee.find(callback).limit(limit);
}

module.exports.validateUser=function (userName,password,callback) {
    var query={userName:userName,
    password:password,mobileNumber:61413550498};
console.log("query===="+query)
   Employee.findOne(query,function (err,document) {
       console.log("document---"+document._id)
   });




};
