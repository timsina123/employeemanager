// constants definition
const  express =require("express");
const bodyPareser=require("body-parser");
const ejs=require("ejs");
const Nexmo=require("nexmo");
const socketio=require("socket.io");
const mongoose=require("mongoose");
const bcrypt=require('bcrypt');


Employee=require('./models/users');

// init app
const app=express();

// init nexmo

const nexmo=new Nexmo({

        apiKey:'6511e122',
        apiSecret:'0d37b7805b2134c2'
    },
    {debug:true});

// body parser middleware
app.use(bodyPareser.urlencoded({
    extended:true
}));
app.use(bodyPareser.json());

// setting view engine
app.set('view engine','html');
app.engine('html',ejs.renderFile);
app.use(express.static(__dirname+'/public'));

// routes
app.get('/',function (req,res) {
    res.render('index');
});





// catching user create post request

app.post('/application/user',function (req,res) {
    var user={userName:req.body.userName,password:req.body.password,mobileNumber:req.body.mobileNumber};

    Employee.addUser(user,function (err) {
if(err){
    throw err;
}
else{
    console.log('New user created successfully');
}
res.redirect('/application/users')
    });
});

// catching post request for mobile verification

app.post('/application/verify',function (req,res) {
    const number=req.body.number;
    const text=req.body.text;

    nexmo.message.sendSms(
        'NEXMO',number,text,{type:'unicode'},function(err,responseData){
            if(err){
                throw err;
            }
            else {
                console.dir(responseData);
            }
        }
    )

})

// getting users

app.get('/application/users',function (req,res) {
    Employee.getUsers(function (err,users) {
        if(err){
            throw err;
        }
        else {
            res.render('usersList.ejs',{users});
        }
    })

});
app.post('/application/validate',function (req,res) {
    var userName=req.body.userName;
    var password=req.body.password;
    Employee.findOne({userName:userName}, {}, function(err, doc) {
        if(err){
            throw err;
        }
        else{
            if(doc){
                bcrypt.compare(password, doc.password, function(err, res) {
                    if(err){
                        throw err;}
                    else {
                        if(res){
                            console.log("Log in successfull");
                        }
                        else {
                            console.log("Invalid  password")
                        }

                    }
                });
            }
            else{
                console.log("Invalid  User Name")
            }

        }

    });




})
app.get('/application/login',function (re,res) {
    res.render('login.html');

});

// mongoose connection

mongoose.connect('mongodb://localhost/userData');
var db=mongoose.connection;

// port and server listen
const port=3000;
const  server=app.listen(port,function(){console.log("server started on port: "+port)});

// connection to socket io

const io=socketio(server);
io.on('connection',function(){
    console.log('connected');
    io.on('disconnect',function(){
        console.log('disconnected');
});
});